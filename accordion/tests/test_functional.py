from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import os
import time

class TestFunctional(StaticLiveServerTestCase):

    def setUp(self):
        super().setUp()

        chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument("--headless")
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.close()
        super().tearDown()

    def test_accordion_rendered(self):
        self.browser.get(self.live_server_url)
        accordion = self.browser.find_element_by_css_selector(".accordion_wrapper")
        self.assertNotEqual(accordion.value_of_css_property("display"), 'none')

    def test_accordion_child_rendered(self):
        self.browser.get(self.live_server_url)
        accordion_items = self.browser.find_elements_by_css_selector(".accordion_item")
        for item in accordion_items:
            self.assertNotEqual(item.value_of_css_property("display"), 'none')

    def test_accordion_clicked_goes_up(self):
        self.browser.get(self.live_server_url)
        accordion_items = self.browser.find_elements_by_css_selector(".accordion_item")
        selected_accor = accordion_items[1]
        butt = selected_accor.find_element_by_css_selector("button[data-toggle-up]")
        butt.click()

        accor_newlist = self.browser.find_elements_by_css_selector(".accordion_item")
        self.assertEqual(selected_accor, accor_newlist[0])

    def test_accordion_clicked_goes_down(self):
        self.browser.get(self.live_server_url)
        accordion_items = self.browser.find_elements_by_css_selector(".accordion_item")
        selected_accor = accordion_items[0]
        butt = selected_accor.find_element_by_css_selector("button[data-toggle-down]")
        butt.click()

        accor_newlist = self.browser.find_elements_by_css_selector(".accordion_item")
        self.assertEqual(selected_accor, accor_newlist[1])

    def test_theme_functionality(self):
        self.browser.get(self.live_server_url)
        root = self.browser.find_element_by_css_selector('.page_body')
        toggle = self.browser.find_element_by_css_selector('.toggle-theme')
        toggle.click()
        get_body = self.browser.find_element_by_css_selector(".theme-dark")

        self.assertEqual(root, get_body)
